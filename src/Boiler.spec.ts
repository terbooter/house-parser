import {expect} from "chai"
import "mocha"
import {nodesToString, stringToNodes} from "./functions"
import {Boiler} from "./Boiler"

const boiler = new Boiler()

describe("Boiler", () => {
    it("should replace all double spaces to single", () => {
        const source = "S  s   s     s    s s s s  s "
        let nodes = boiler.boil(stringToNodes(source))
        expect(nodesToString(nodes)).to.equal("S s s s s s s s s ")
    })
})
