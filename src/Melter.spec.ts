import {expect} from "chai"
import "mocha"
import {Melter} from "./Melter"
import {stringToNodes} from "./functions"

const melter = new Melter()

describe("Melter", () => {
    it("should find simple number", () => {
        const source = "text 34  more text"
        let nodes = melter.melt(stringToNodes(source))
        expect(nodes[0].value).to.equal("34")
    })

    it("should find number with slash", () => {
        const source = "text 23/2 34  more text"
        let nodes = melter.melt(stringToNodes(source))
        expect(nodes[0].value).to.equal("23/2")
    })

    it("should find closed intervals", () => {
        const source = "we 1-20 df"
        let nodes = melter.melt(stringToNodes(source))
        expect(nodes[0].intervalData.begin).to.equal(1)
        expect(nodes[0].intervalData.end).to.equal(20)
    })

    it("should find modificator", () => {
        const source = "we четные 10-20 df"
        let nodes = melter["meltCharsToModificatorIfPossible"](stringToNodes(source), "четные")
        expect(nodes[3].value).to.equal("четные")
        expect(nodes[3].type).to.equal("modificator")
    })
})
