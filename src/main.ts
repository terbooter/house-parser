import {HouseParser} from "./HouseParser"

console.log("Run 'npm run test' for more tests")

main()

function main() {
    runTests()
}

function runTests() {
    const sources = [
        "четные 2-28, нечетные 1-21 7/1, 11; 5 102-110",
        "четные 2-28, нечетные 1-21;7/1, 11, 17,",
        "нечетные 11+, четные 42+",
        "четные с 20 и вся улица до конца",
        "7/1, 11, 17, 17/1, 17/2, 8/2, 15, 15/1, 15а",
        "12, 22, 36, 42, 45, 100-106"
    ]

    // let parser = new HouseParser(sources[0])
    // let result = parser.isHouseIncluded("5")
    // console.log();

    runSingleTest(sources[0], "5")

    runSingleTest(sources[0], "55")

    runSingleTest(sources[0], "7")
    runSingleTest(sources[0], "7/1")
    runSingleTest(sources[0], "101")
    runSingleTest(sources[0], "102")
    runSingleTest(sources[0], "103")
    runSingleTest(sources[0], "120")

    runSingleTest(sources[0], "6")
    runSingleTest(sources[0], "27")
    return

    const houseNumbers = ["1", "2", "9", "7/1", "15", "15a", "42", "106"]

    for (const source of sources) {
        for (const number of houseNumbers) {
            runSingleTest(source, number)
        }
    }
}

function runSingleTest(source: string, houseNumber: string) {
    let parser = new HouseParser(source)
    let result = parser.isHouseIncluded(houseNumber)
    console.log(` ==>>>${source} - ${houseNumber} - ${result}`)
}
