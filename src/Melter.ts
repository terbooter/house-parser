import {Node} from "./interfaces"
import {getNodeIndexById, isInteger} from "./functions"

const availableNumberChars = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "/", "a", "б", "в"]

export class Melter {
    public melt(nodes: Node[]): Node[] {
        const {
            doAll,
            meltNumberChars,
            convertCharToNumberIfPossible,
            meltClosedInterval,
            meltCharsToModificatorIfPossible,
            boilCharNode,
            meltModificatorToInterval
        } = this

        nodes = doAll(nodes, meltNumberChars)
        nodes = doAll(nodes, convertCharToNumberIfPossible)
        nodes = doAll(nodes, meltClosedInterval)
        nodes = doAll(nodes, meltCharsToModificatorIfPossible, "нечетные")
        nodes = doAll(nodes, meltCharsToModificatorIfPossible, "четные")
        nodes = doAll(nodes, boilCharNode)
        nodes = doAll(nodes, meltModificatorToInterval)

        return nodes
    }

    private meltNumberChars(nodes: Node[]): Node[] {
        for (let i = 0; i < nodes.length - 1; i++) {
            const node = nodes[i]
            const nextNode = nodes[i + 1]

            const isNodeNumber =
                node.type === "number" || (node.type === "char" && availableNumberChars.includes(node.value))
            const isNextNodeNumber =
                nextNode.type === "number" ||
                (nextNode.type === "char" && availableNumberChars.includes(nextNode.value))

            if (isNodeNumber && isNextNodeNumber) {
                node.type = "number"
                node.value += nextNode.value
                nodes.splice(i + 1, 1)
                return nodes
            }
        }

        return nodes
    }

    private convertCharToNumberIfPossible(nodes: Node[]): Node[] {
        return nodes.map((node: Node) => {
            if (isInteger(node.value) && node.value.length === 1) {
                return {...node, type: "number"}
            }

            return node
        })
    }

    private meltCharsToModificatorIfPossible(nodes: Node[], modificatorString: "четные" | "нечетные"): Node[] {
        let charString = ""
        let indexToId: number[] = []
        for (const node of nodes) {
            if (node.type === "char") {
                charString += node.value
                indexToId.push(node.id)
            }
        }

        const index = charString.indexOf(modificatorString)
        if (index === -1) {
            return nodes
        }

        const nodeId = indexToId[index]
        const nodeIndex = getNodeIndexById(nodes, nodeId)
        const node = nodes[nodeIndex]

        node.type = "modificator"
        if (modificatorString === "нечетные") {
            node.modificatorType = "odd"
        } else if (modificatorString === "четные") {
            node.modificatorType = "even"
        } else {
            throw new Error(`Unknown modificator: ${modificatorString}`)
        }
        node.value = modificatorString

        for (let i = index + 1; i < index + modificatorString.length; i++) {
            const nodeId = indexToId[i]
            const nodeIndex = getNodeIndexById(nodes, nodeId)
            nodes.splice(nodeIndex, 1)
        }

        return nodes
    }

    private meltClosedInterval(nodes: Node[]): Node[] {
        for (let i = 1; i < nodes.length - 1; i++) {
            const previousNode = nodes[i - 1]
            const node = nodes[i]
            const nextNode = nodes[i + 1]

            if (node.value === "-") {
                const begin = parseInt(previousNode.value)
                const end = parseInt(nextNode.value)
                const intervalSource = `${previousNode.value}-${nextNode.value}`

                if (isNaN(begin) || isNaN(end)) {
                    throw new Error(`Bad source format. Can not parse interval from source ${intervalSource}`)
                }

                previousNode.type = "interval"
                previousNode.value = intervalSource
                previousNode.intervalData = {begin, end, modificatorType: null}

                nodes.splice(i, 2)
                return nodes
            }
        }

        return nodes
    }

    private meltOpenedInterval(nodes: Node[]): Node[] {
        return null
    }

    private boilCharNode(nodes: Node[]): Node[] {
        for (let i = 0; i < nodes.length; i++) {
            if (nodes[i].type === "char") {
                nodes.splice(i, 1)
                return nodes
            }
        }
        return nodes
    }

    private meltModificatorToInterval(nodes: Node[]): Node[] {
        for (let i = 0; i < nodes.length - 1; i++) {
            const node = nodes[i]
            const nextNode = nodes[i + 1]
            if (node.type === "modificator" && nextNode.type === "interval") {
                const modificatorType = node.modificatorType
                node.type = "interval"
                node.value = `${node.value}${nextNode.value}`
                node.intervalData = nextNode.intervalData
                node.intervalData.modificatorType = modificatorType
                delete node.modificatorType
                nodes.splice(i + 1, 1)
                return nodes
            }
        }
        return nodes
    }

    private doAll(nodes: Node[], method: (nodes: Node[], param?) => Node[], param?): Node[] {
        while (true) {
            const countBefore = nodes.length
            nodes = method(nodes, param)
            const countAfter = nodes.length
            if (countBefore === countAfter) {
                break
            }
        }

        return nodes
    }
}
