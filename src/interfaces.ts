export interface Node {
    type: "char" | "number" | "modificator" | "interval"
    id: number
    value: string
    intervalData?: IntervalData
    modificatorType?: ModificatorType
}

export interface IntervalData {
    begin: number
    end: number
    modificatorType?: ModificatorType
}

export type ModificatorType = "odd" | "even"
