import {Node} from "./interfaces"

export function stringToNodes(source: string): Node[] {
    let arr: string[] = []
    for (let i = 0; i < source.length; i++) {
        arr.push(source.charAt(i))
    }

    return arr.map((char: string, index: number) => {
        const node: Node = {id: index, type: "char", value: char}
        return node
    })
}

export function nodesToString(nodes: Node[]): string {
    let str = ""
    for (let node of nodes) {
        str += node.value
    }
    return str
}

export function isInteger(value): boolean {
    return !isNaN(parseInt(value))
}

export function getNodeIndexById(nodes: Node[], id: number): number {
    for (let i = 0; i < nodes.length; i++) {
        if (nodes[i].id === id) {
            return i
        }
    }

    return null
}
