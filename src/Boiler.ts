import {Node} from "./interfaces"

export class Boiler {
    public boil(nodes: Node[]): Node[] {
        const {boilAll, boilDoubleSpaces, boilLeftSpace, boilRightSpace} = this

        nodes = boilAll(nodes, boilDoubleSpaces)
        nodes = boilAll(nodes, boilLeftSpace, "/")
        nodes = boilAll(nodes, boilRightSpace, "/")
        nodes = boilAll(nodes, boilLeftSpace, "-")
        nodes = boilAll(nodes, boilRightSpace, "-")
        nodes = boilAll(nodes, boilLeftSpace, "+")

        return nodes
    }

    private boilAll(nodes: Node[], method: (nodes: Node[], param?) => Node[], param?): Node[] {
        while (true) {
            const countBefore = nodes.length
            nodes = method(nodes, param)
            const countAfter = nodes.length
            if (countBefore === countAfter) {
                break
            }
        }

        return nodes
    }

    private boilDoubleSpaces(nodes: Node[]): Node[] {
        for (let i = 0; i < nodes.length - 1; i++) {
            const node = nodes[i]
            const nextNode = nodes[i + 1]
            if (node.value === " " && nextNode.value === " ") {
                node.type = "char"
                nodes.splice(i + 1, 1)
                return nodes
            }
        }

        return nodes
    }

    private boilLeftSpace(nodes: Node[], char: string): Node[] {
        for (let i = 1; i < nodes.length; i++) {
            const node = nodes[i]
            const previousNode = nodes[i - 1]
            if (node.value === char && previousNode.value === " ") {
                nodes.splice(i - 1, 1)
                return nodes
            }
        }

        return nodes
    }

    private boilRightSpace(nodes: Node[], char: string): Node[] {
        for (let i = 0; i < nodes.length - 1; i++) {
            const node = nodes[i]
            const nextNode = nodes[i + 1]
            if (node.value === char && nextNode.value === " ") {
                nodes.splice(i + 1, 1)
                return nodes
            }
        }

        return nodes
    }
}
