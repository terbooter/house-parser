import {Node} from "./interfaces"
import {Boiler} from "./Boiler"
import {nodesToString, stringToNodes} from "./functions"
import {Melter} from "./Melter"

export class HouseParser {
    private boiler = new Boiler()
    private melter = new Melter()
    private nodes: Node[]

    constructor(private readonly source: string) {
        source = source.toLowerCase()
        let nodes: Node[] = stringToNodes(source)
        nodes = this.boiler.boil(nodes)
        nodes = this.melter.melt(nodes)
        // console.log(nodes)
        // console.log(source)
        // console.log(nodesToString(nodes))

        this.nodes = nodes
    }

    public isHouseIncluded(houseNumber: string): boolean {
        houseNumber = houseNumber.toLowerCase()
        let nodes: Node[] = stringToNodes(houseNumber)
        nodes = this.boiler.boil(nodes)
        houseNumber = nodesToString(nodes)

        if (this.isHouseInNumberNodes(houseNumber)) {
            return true
        }

        const intervals: Node[] = this.nodes.filter((node) => {
            if (node.type === "interval") {
                return true
            }

            return false
        })

        for (let node of intervals) {
            if (this.isHouseInIntervalNode(node, houseNumber)) {
                return true
            }
        }

        return false
    }

    private isHouseInNumberNodes(houseNumber: string): boolean {
        const numberNode = this.nodes.find((node) => {
            if (node.type === "number" && node.value === houseNumber) {
                return true
            }
        })

        if (numberNode) {
            return true
        }

        return false
    }

    private isHouseInIntervalNode(node: Node, houseNumber: string): boolean {
        const house = parseInt(houseNumber)
        if (isNaN(house)) {
            throw new Error(`Find in interval error. House number ${houseNumber} can not be converted to integer`)
        }

        const {begin, end, modificatorType} = node.intervalData

        if (house < begin) {
            return false
        }

        if (house > end) {
            return false
        }

        if (!modificatorType) {
            return true
        }

        if (modificatorType === "odd" && this.typeOfNumber(house) === "odd") {
            return true
        }

        if (modificatorType === "even" && this.typeOfNumber(house) === "even") {
            return true
        }

        return false
    }

    private typeOfNumber(value: number): "odd" | "even" {
        if (value % 2 === 0) {
            return "even"
        }

        return "odd"
    }
}
