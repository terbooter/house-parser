import {Node} from "./interfaces"
import {expect} from "chai"
import "mocha"
import {HouseParser} from "./HouseParser"

describe("HouseParser", () => {
    it("should find regular numbers", () => {
        const source = "четные 2-28, нечетные 1-21 7/1, 11; 5"
        let parser = new HouseParser(source)

        expect(parser.isHouseIncluded("5")).to.equal(true)
        expect(parser.isHouseIncluded("11")).to.equal(true)
        expect(parser.isHouseIncluded("7/1")).to.equal(true)

        expect(parser.isHouseIncluded("7/11")).to.equal(false)
        expect(parser.isHouseIncluded("55")).to.equal(false)
        expect(parser.isHouseIncluded("1")).to.equal(false)
    })

    it("should find numbers in intervals", () => {
        const source = "четные 10-28, нечетные 1-21 7/1, 11; 5   102-110"
        let parser = new HouseParser(source)

        expect(parser.isHouseIncluded("5")).to.equal(true)
        expect(parser.isHouseIncluded("101")).to.equal(false)
        expect(parser.isHouseIncluded("102")).to.equal(true)
        expect(parser.isHouseIncluded("103")).to.equal(true)
        expect(parser.isHouseIncluded("111")).to.equal(false)
        expect(parser.isHouseIncluded("2")).to.equal(false)
        expect(parser.isHouseIncluded("3")).to.equal(true)
        expect(parser.isHouseIncluded("27")).to.equal(false)
        expect(parser.isHouseIncluded("11")).to.equal(true)
        expect(parser.isHouseIncluded("7/1")).to.equal(true)

        // TODO Bug here!
        // expect(parser.isHouseIncluded("7/11")).to.equal(false)
    })

    it(".isHouseInIntervalNode should find number in closed interval", () => {
        const source = ""
        const parser = new HouseParser(source)
        const node: Node = {
            id: 0,
            type: "interval",
            value: "четные2-28",
            intervalData: {begin: 2, end: 28, modificatorType: "even"}
        }

        let result = parser["isHouseInIntervalNode"](node, "2")
        expect(result).to.equal(true)

        result = parser["isHouseInIntervalNode"](node, "3")
        expect(result).to.equal(false)

        result = parser["isHouseInIntervalNode"](node, "26")
        expect(result).to.equal(true)

        result = parser["isHouseInIntervalNode"](node, "30")
        expect(result).to.equal(false)
    })
})
