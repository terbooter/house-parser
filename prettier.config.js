module.exports = {
    trailingComma: "none",
    tabWidth: 4,
    printWidth: 120,
    semi: false,
    singleQuote: false,
    jsxBracketSameLine: true,
    bracketSpacing: false,
    arrowParens: "always",
    endOfLine: "lf"
}
